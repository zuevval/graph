from utils import prepare_graph
from dfs import dfs
from bfs import bfs
from display import draw_steps


def demo():
    g = prepare_graph()
    used = dfs(g)
    draw_steps(g, used, "depth first search")
    used = bfs(g)
    draw_steps(g, used, "breadth first search")


if __name__ == "__main__":
    demo()
