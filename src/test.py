from collections import defaultdict
from dfs import dfs
from bfs import bfs
from utils import prepare_graph
import unittest


class BfsDfsTests(unittest.TestCase):
    def test_dfs_empty_input(self):
        g = defaultdict(list)
        res = dfs(g)
        assert len(res) == 0

    def test_dfs_five_nodes(self):
        g = prepare_graph()
        res = dfs(g)
        assert len(res) == 5
        for v in ["A", "B", "C", "D", "E"]:
            assert v in res

    def test_bfs_empty_input(self):
        g = defaultdict(list)
        res = bfs(g)
        assert len(res) == 0

    def test_bfs_five_nodes(self):
        g = prepare_graph()
        res = bfs(g)
        assert len(res) == 5
        for v in ["A", "B", "C", "D", "E"]:
            assert v in res
