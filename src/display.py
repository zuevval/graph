from typing import DefaultDict, List
import networkx as nx  # type: ignore
import matplotlib.pyplot as plt  # type: ignore


def _draw_graph(g: DefaultDict[str, List[str]], used: List[str]) -> None:
    gx = nx.Graph()
    for v in used:
        for u in g[v]:
            if u in used:
                gx.add_edge(v, u)
        else:
            gx.add_node(v)
    pos = nx.spring_layout(gx)
    nx.draw(gx, pos)
    nx.draw_networkx_labels(gx, pos, font_size=16)
    plt.show()


def draw_steps(g: DefaultDict[str, List[str]], used: List[str],
               msg: str = "") -> None:
    for i in range(1, len(used) + 1):
        plt.title(msg + "\n step " + str(i) + ", traversed vertices: "
                  + str(used[:i]) + "\n to go to next step, close this window")
        _draw_graph(g, used[:i])
