from typing import DefaultDict, List, Set


def bfs(g: DefaultDict[str, List[str]]) -> Set[str]:
    used = set()
    stack = []
    for v in g:
        stack.append(v)
    while stack:
        v = stack.pop()
        if v not in used:
            used.add(v)
            stack.extend(g[v])
    return used
