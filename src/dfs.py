from typing import DefaultDict, List
from collections import deque


def dfs(g: DefaultDict[str, List[str]]) -> List[str]:
    used = []
    queue: deque = deque()
    for v in g:
        queue.append(v)
    while queue:
        v = queue.popleft()
        if v not in used:
            used.append(v)
            queue.extend(g[v])
    return used
