from collections import defaultdict
from typing import DefaultDict, List


def prepare_graph() -> DefaultDict[str, List[str]]:
    g = defaultdict(list)
    g["A"].extend(["B", "C", "D", "E"])
    g["C"].extend(["B", "D", "E"])
    g["D"].extend(["A", "B", "E"])
    return g
