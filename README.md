# graph
## Purpose
visualization of algorithms on graph: bread first search, depth first search
## Requirements
- python 3.7.x or higher
- additional packages listed in `requirements.txt`
## Usage
- check out this repository locally: `git clone https://gitlab.com/zuevval/graph.git`
- from the root folder of repository: `[your_python3_command] src/main.py` 
(for example, in Windows: `python src/main.py`)
- you will see algorithm's steps one by one, Breadth First Search and Depth First Search.
 To navigate to the next step, close current window.
 
## Explanation
Those screens visualize DFS and BFS traverses of an example graph defined by
its vertices adjacency lists in `utils.py`. Each edge is treated as not oriented,
edge may or may not occur twice in a graph (algorithms will handle both cases 
correctly, though an order of visits might differ).

Both algorithms start search from vertices that are keys in adjacency lists. 
For each key-vertex, a breadth or depth first search (starting in this vertex)
 is performed. If the vertex was visited in a previous run, it is skipped. Thus,
 a connected graph will be traversed in one run starting from one key, 
 disconnected - in several runs. 

One screen corresponds to one vertex added to 'visited'. On each screen, a sub-graph 
formed by all visited vertices and all edges between them is shown.

## Example
see "Usage".
